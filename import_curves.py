import bpy
from .import_constraints import unpack_data
from .import_deformers import import_modifiers

def locate_or_create_deform():
	deform = [c for c in bpy.data.collections if (c.name.endswith("_deform"))]

	if len(deform) == 0:
		asset_collection = bpy.context.scene.collection.children[0]
		base_name = asset_collection.name
		deform = bpy.data.collections.new(base_name + "_deform")
		asset_collection.children.link(deform)
		return deform
	else:
		return deform[0]

	return None


def import_bezier_points(spline, geometry):
	nb_pts = len(geometry) - 1
	spline.bezier_points.add(nb_pts)

	for point_obj, point_data in zip(spline.bezier_points, geometry):
		unpack_data(point_obj, point_data)


def import_nurbs_points(spline, geometry):
	nb_pts = len(geometry) - 1
	spline.points.add(nb_pts)

	for point_obj, point_data in zip(spline.points, geometry):
		unpack_data(point_obj, point_data)


def import_curve_splines(curve_raw_data, curve_data):
	for spline_raw_data in curve_raw_data['splines']:
		spline = curve_data.splines.new(spline_raw_data['type'])

		if spline_raw_data['type'] == 'BEZIER':
			import_bezier_points(spline, spline_raw_data['data']['geometry'])

		else:
			import_nurbs_points(spline, spline_raw_data['data']['geometry'])

		unpack_data(spline, spline_raw_data['data']['basis'])



def import_curve(deform_collection, curve_raw_data):
	name  = curve_raw_data['basis']['name']
	curve_data   = bpy.data.curves.new(name, 'CURVE')
	curve_object = bpy.data.objects.new(name, curve_data)
	deform_collection.objects.link(curve_object)

	unpack_data(curve_object, curve_raw_data['basis'])
	unpack_data(curve_data, curve_raw_data['data'])

	import_curve_splines(curve_raw_data, curve_data)
	
	return curve_object


def hook_curve(curve_raw_data, curve_object):
	bpy.context.view_layer.objects.active = curve_object
	bpy.ops.object.mode_set(mode='EDIT')
	import_modifiers(curve_object, curve_raw_data['modifiers'])
	bpy.ops.object.mode_set(mode='OBJECT')


def import_curves(modules_imported, curves_data):
	active = bpy.context.view_layer.objects.active
	imported_curves = []
	
	for i, curve_data in enumerate(curves_data):
		if curve_data['module'] in modules_imported:
			deform = locate_or_create_deform()
			if deform is None:
				break
			c = import_curve(deform, curve_data)
			imported_curves.append((c, i))

	bpy.context.view_layer.objects.active = active

	return imported_curves


def hook_curves(curves_instances, curves_data):
	active = bpy.context.view_layer.objects.active

	for curve, index in curves_instances:
		hook_curve(curves_data[index], curve)

	bpy.context.view_layer.objects.active = active