import bpy
from .utils import ENV

# ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !
# /!\  Skip that file if you like clean GUIs  /!\
# ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !

def properly_named():
    name_collection = bpy.context.scene.collection.children[0].name
    name_armature = bpy.context.object.name

    return (name_collection + "_rig") == name_armature


class RigTransferPanel(bpy.types.Panel):

    """ Panel used by Rig Transfer addon """

    bl_label       = "Rig Modules Transfer"
    bl_idname      = "OBJECT_PT_rig_transfer"
    bl_space_type  = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context     = "object"

    @classmethod
    def poll(cls, context):
        return (context.mode == 'OBJECT') and (context.object is not None) and (context.object.type == 'ARMATURE')


    def draw(self, context):
        layout = self.layout
        obj = context.object

        if not properly_named():
            row = layout.row()
            row.alert = True
            row.label(icon='ERROR', text="Armature not properly named")

        # Target of all operations
        split = layout.split(factor=0.92, align=True)
        split.alert = not properly_named()
        split.prop(obj, "name", text="", icon='OUTLINER_OB_ARMATURE')
        split.operator('object.rename_armature_export_rig', text="", icon='SHADERFX')

        # Import or Export ?
        row = layout.row()
        row.prop(context.scene.rig_transfer, 'enum_operations', expand=True)

        layout.separator()

        # If we are importing modules
        if context.scene.rig_transfer.enum_operations == 'IMPORT':

            # Where the source is fetched from (JSON or BLEND file path), in red if not valid
            split = layout.split(align=True)
            if not context.scene.rig_transfer.valid_file:
                split.alert = True
            split.prop(context.scene.rig_transfer, 'file_path', text="")

            #layout.separator()

            # Research bar to probe shared modules
            split = layout.split(factor=0.1, align=True)
            split.operator("object.rig_transfer_probe_module", text="", icon='VIEWZOOM')
            split = split.split(factor=0.9, align=True)
            data = context.scene.rig_transfer.pool_probe_modules
            split.prop_search(data, 'look_for', data, 'look_in', text="")
            split = split.split(align=True)
            split.operator("object.rig_transfer_transfer_path", text="", icon='SORT_DESC')
            layout.separator()

            split = layout.split(factor=0.1, align=True)
            split.prop(context.scene.rig_transfer, 'generic_name', text="", icon='PASTEDOWN')

            # Button of the importer (parsing JSON/BLEND)
            split.operator("object.rig_transfer_import")

            layout.separator()

            all = bpy.context.scene.rig_transfer.probed_data[0] if len(bpy.context.scene.rig_transfer.probed_data) > 0 else None

            # If some modules were found in the source file
            if all is not None:
                # Comparing JSON major version to current major version
                if 'version_found' in ENV:
                    if int(ENV['version_found']) < int(ENV['version']):
                        row = layout.row()
                        row.alert = True
                        row.label(text="This JSON version is too old ({})".format(ENV['version_found']), icon='ERROR')
                        return
                
                    
                for found_module in context.scene.rig_transfer.probed_data:
                    split_main = layout.split(align=True, factor=0.9)
                    # If ALL is checked, all other boxes are gray
                    if (found_module.name_module != 'ALL') and (all.imported):
                        split_main.enabled = False
                    
                    # Place the icon that indicates if a module is already imported
                    modules = [g.name for g in context.object.pose.bone_groups]
                    split = split_main.split(factor=0.1, align=True)
                    if found_module.name_module == 'ALL':
                        split.label(text="", icon='BLANK1')
                    else:
                        if found_module.name_module.lower()+".module" in modules:
                            split.label(text="", icon='RADIOBUT_ON')
                            split.enabled = False
                        else:
                            split.label(text="", icon='RADIOBUT_OFF')

                    # Place the checkbox
                    split = split.split(factor=0.1, align=True)
                    split.prop(found_module, 'imported', text="")

                    # Place the text
                    split = split.split(factor=0.8, align=True)
                    disp = found_module.name_module # disp = name of module without '.module' and a capital letter
                    if disp in ENV['groups']:
                        disp += " · ({} bones)".format(ENV['groups'][disp])
                    split.label(text=disp)

                    # Place the 'delete module' button
                    split = split_main.split()
                    
                    if found_module.name_module == 'ALL':
                        pass #split.label(text="", icon='BLANK1')
                    else:
                        split.operator('object.remove_module_bones', text="", icon='TRASH').module_name = found_module.name_module.lower()+".module"
                        if not found_module.name_module.lower()+".module" in modules:
                            split.enabled = False

                layout.separator()
                split = layout.split(align=True, factor=0.05)
                split.prop(context.scene.rig_transfer, 'all_differentials', text="")
                split.operator("object.rig_transfer_apply", icon='PLUS')

                # Alert is the 'import all differential' thing is checked
                if context.scene.rig_transfer.all_differentials:
                    row = layout.row()
                    row.alert = True
                    row.label(text="All differential bones will be imported", icon='BONE_DATA')


        # We are exporting modules
        else:
            split = layout.split(factor=0.1, align=True)
            
            if not context.scene.rig_transfer.valid_export_dir:
                split.alert = True
            
            split.operator('object.select_dir_exp_json', text="", icon='FILEBROWSER')

            split = split.split(factor=0.9, align=True)
            if not context.scene.rig_transfer.valid_export_file:
                split.alert = True
            split.prop(context.scene.rig_transfer, 'export_file_name', icon_only=True)
            split.operator("object.auto_name_export_rig", icon='SHADERFX', text='')

            row = layout.row()
            path = context.scene.rig_transfer.path_dir_export + context.scene.rig_transfer.export_file_name if context.scene.rig_transfer.path_dir_export[-1] in ['/', '\\'] else context.scene.rig_transfer.path_dir_export + '/' + context.scene.rig_transfer.export_file_name
            row.label(text=path)

            split = layout.split(factor=0.1, align=True)
            split.prop(context.scene.rig_transfer, 'generic_name', text="", icon='COPYDOWN')

            split.operator("object.rig_transfer_export")



