import bpy
from .extract_empties import subrig_collection
from .WarningCollector import WARNINGS_COLLECTOR
from .import_constraints import unpack_data
from math import pow, sqrt

def create_subrig_collection():
	base_name = bpy.context.scene.collection.children[0].name
	rubrig_name = base_name + "_subrig"
	rig_collection = bpy.data.collections.get(base_name + "_rig")

	if rig_collection is None:
		WARNINGS_COLLECTOR.add_warning("Import of empties skipped", "Empties have not been imported, collection {} has not been found".format(base_name + "_rig"))
		return None
	else:
		subrig = bpy.data.collections.new(base_name + "_subrig")
		rig_collection.children.link(subrig)
		return subrig

def get_subrig_collection():
	subrig = subrig_collection()
	if subrig is None:
		subrig = create_subrig_collection()
	
	return subrig


def distance(p1, p2):
	x = pow(p1[0] - p2[0], 2)
	y = pow(p1[1] - p2[1], 2)
	z = pow(p1[2] - p2[2], 2)
	return sqrt(x + y + z)


def closest_point(object, position):
	point = (0.0, 0.0, 0.0)
	index = -1
	min_dist = -1.0

	for i, v in enumerate(object.data.vertices):
		if (index == -1):
			index = 0
			point = tuple(v.co)
			min_dist = distance(v.co, position)
		else:
			if (distance(v.co, position) < min_dist):
				min_dist = distance(v.co, position)
				index = i
				point = tuple(v.co)

	return {'min_dist': min_dist, 'point': point, 'index': index}


def assign_and_check_parents(empty, empty_data):
	for i, vertex in enumerate(empty_data['positions']):
		index = vertex['index']
		co = vertex['co']
		parent = empty.parent

		if parent is None:
			return

		if parent is None:
			WARNINGS_COLLECTOR.add_warning("Impossible to set property", "Parent not set in {}, cannot set up parenting properties".format(empty.name))

		if (index >= len(parent.data.vertices)) or (distance(parent.data.vertices[index].co, co) > 0.01):
			WARNINGS_COLLECTOR.add_warning("Impossible to set property", "Index of vertex too high in {}, tried to assign to other vertices".format(empty.name))
			closest = closest_point(parent, co)
			correct_index = closest['index']
		else:
			correct_index = index

		empty.parent_vertices[i] = correct_index



def import_empty(subrig, empty_data):
	empty = bpy.data.objects.new(name=empty_data['data']['name'], object_data=None)
	subrig.objects.link(empty)
	unpack_data(empty, empty_data['data'])
	assign_and_check_parents(empty, empty_data)


def import_empties(modules_imported, empties_data):
	for empty_data in empties_data:
		if empty_data['module'] in modules_imported:
			subrig = get_subrig_collection()
			if subrig is None:
				break
			import_empty(subrig, empty_data)