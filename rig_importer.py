import bpy
import json
from math import isclose
from mathutils import Vector, Matrix, Color
from .utils import ENV
from .import_deformers import import_deformers
from .WarningCollector import WARNINGS_COLLECTOR
from .manage_cs import fetch_cs_collection, import_custom_shape

from .import_constraints import import_constraints, constraint_from_datapath, rebuild_driver, postprocess, unpack_data
from .import_empties import import_empties
from .import_curves import import_curves, hook_curves

rig_import_found_data = {}

# Function that loads a JSON and parse it into a Python dictionary
def rig_importer():
    global rig_import_found_data

    path = bpy.path.abspath(bpy.context.scene.rig_transfer.file_path)
    file = open(path, 'r')
    to_read = file.read()

    generic = bpy.context.scene.rig_transfer.generic_name
    asset_name = bpy.context.scene.collection.children[0].name
    
    if generic:
        to_read = to_read.replace('<asset_name>', asset_name)

    data = json.loads(to_read)
    rig_import_found_data = data
    file.close()

    found = bpy.context.scene.rig_transfer.probed_data
    # Add 'ALL' checkbox to not import modules but every single differential bone
    item = found.add()
    item.set("ALL", False)

    try:
        version_found = data['version_exporter']
    except:
        version_found = -1

    # Detect all modules contained within the JSON file, expose them to the GUI
    for name, content in data['modules'].items():
        item = found.add()
        new_name = name.replace(".module", "").capitalize()
        item.set(new_name, False)
        ENV.setdefault('groups', {})
        ENV['groups'][new_name] = len(content['bones'])

    return version_found


def get_imported_data():
    global rig_import_found_data
    return rig_import_found_data

def rig_apply():
    global rig_import_found_data

    try:
        # The ALL button
        all = bpy.context.scene.rig_transfer.probed_data[0]
    except:
        return

    allowed = {} # Will contain bones that are in the modules that we want to import
    modules_imported = []
    all_differentials = bpy.context.scene.rig_transfer.all_differentials

    for item in bpy.context.scene.rig_transfer.probed_data:
        if (item.name_module != "ALL") and ((item.imported) or (all.imported)):
            full_name = item.name_module.lower() + ".module"
            WARNINGS_COLLECTOR.add_warning("Module(s) imported", "Module '{}' was imported".format(full_name))
            allowed.update(dict.fromkeys(rig_import_found_data['modules'][full_name]['bones'], full_name))
            WARNINGS_COLLECTOR.add_warning("Bone(s) imported", "Bones '{}' were imported".format(", ".join(rig_import_found_data['modules'][full_name]['bones'])))
            modules_imported.append(full_name)


    tree = rig_import_found_data['tree']
    bpy.ops.object.mode_set(mode='EDIT')
    edit_bones = bpy.context.object.data.edit_bones
    added = []
    bones_imported = []
    cs_collection = fetch_cs_collection()
    constraints_imported = {}

    # Importing empties
    import_empties(modules_imported, rig_import_found_data['empties'])

    # Importing curves
    imported_curves = import_curves(modules_imported, rig_import_found_data['curves'])

    for bone_name, data in tree.items():
        if (bone_name not in edit_bones) and (bone_name in allowed.keys() or all_differentials):
            if data['differential']: # The bone is not from the Auto Rig Pro
                bone = edit_bones.new(bone_name)
                bone.head = Vector(data['transformations']['head'])
                bone.tail = Vector(data['transformations']['tail'])
                bone.matrix = Matrix(data['transformations']['matrix'])
                bone.head_radius = data['transformations']['head_radius']
                bone.tail_radius = data['transformations']['tail_radius']
                if not isclose(bone.length, data['transformations']['length'], abs_tol=0.001):
                    print("[Rig Transfer] Error in bone length: {} <-> {}".format(bone.length, data['transformations']['length']))
                bone.envelope_distance = data['transformations']['envelope_distance']
                added.append(bone)

    # The hierarchy is rebuilt in another loop to be sure that everything is instanciated
    for bone in added:
        if 'parent' in tree[bone.name]:
            parent = edit_bones.get(tree[bone.name]['parent'])
            if parent is not None:
                bone.parent = parent
        bones_imported.append(bone.name)

    bpy.ops.object.mode_set(mode='OBJECT')
    added.clear() # We quitted the edit mode

    armature_bones = bpy.context.object.pose.bones

    for bone_name in bones_imported:
        pose_bone = armature_bones.get(bone_name)
        bone = pose_bone.bone 
        data = tree[bone_name]

        # Setting visibility layer
        bone.layers = data['layers']

        if 'custom_properties' in data:
            for custom_prop_name, custom_prop_val in data['custom_properties'].items():
                pose_bone[custom_prop_name] = custom_prop_val # No update method for custom ppties, despite the similarity with dict

        # Retrieving custom shape if appropriate
        if 'custom_shape' in data:
            cs = bpy.data.objects.get(data['custom_shape']['cs'])
            if (cs is None) and (data['custom_shape']['cs'] in rig_import_found_data['custom_shapes']):
                cs = import_custom_shape(data['custom_shape']['cs'], rig_import_found_data['custom_shapes'], cs_collection)

            if cs is not None:
                pose_bone.custom_shape = cs

                pose_bone.custom_shape_scale = data['custom_shape']['scale']

                transform_bone = armature_bones.get(data['custom_shape']['transform'])
                if transform_bone is not None:
                    pose_bone.custom_shape_transform = transform_bone

                pose_bone.use_custom_shape_bone_size = data['custom_shape']['scale_bone_length']

                bone.show_wire = data['custom_shape']['wireframe']

        unpack_data(pose_bone, data['side_properties_pose'])
        unpack_data(bone, data['side_properties_arma'])


        # Creating bone constraints
        import_constraints(pose_bone, data['constraints'], constraints_imported)

        # Retrieving bone group (if exists)
        if bone_name in allowed:
            group = bpy.context.object.pose.bone_groups.get(allowed[bone_name])
            if group is None:
                group = bpy.context.object.pose.bone_groups.new(name=allowed[bone_name])
                group.color_set = rig_import_found_data['modules'][allowed[bone_name]]['color']
                if (group.color_set == 'CUSTOM') and ('colors' in rig_import_found_data['modules'][allowed[bone_name]]):
                    group.colors.normal = Color(rig_import_found_data['modules'][allowed[bone_name]]['colors']['normal'])
                    group.colors.select = Color(rig_import_found_data['modules'][allowed[bone_name]]['colors']['select'])
                    group.colors.active = Color(rig_import_found_data['modules'][allowed[bone_name]]['colors']['active'])
                
            pose_bone.bone_group = group
        else:
            WARNINGS_COLLECTOR.add_warning("Lonely bone imported", "Bone '{}' was imported but is not part of any imported module".format(bone_name))
        

    # Recreating drivers on bone constraints
    drivers = rig_import_found_data['drivers']
    for data_path, driver_data in drivers.items():
        constraint, attribute_name = constraint_from_datapath(bpy.context.object, data_path)
        if constraint is not None:
            rebuild_driver(constraint, attribute_name, driver_data)


    # Recreating deformation cages and lattices
    import_deformers(rig_import_found_data, modules_imported)

    hook_curves(imported_curves, rig_import_found_data['curves'])

    bpy.ops.object.mode_set(mode='OBJECT')

    postprocess(constraints_imported)

    bpy.context.scene.rig_transfer.probed_data.clear()