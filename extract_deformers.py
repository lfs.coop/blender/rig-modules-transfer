import bpy
from .extract_constraints import extract_variables, export_driver, export_drivers, one_of
from .import_deformers import locate_deform_collection

# Attributes that we want to import and export from a lattice
from_lattice = ['parent', 'parent_type', 'parent_bone', 'track_axis', 'up_axis', 'type', 'name', 'scale', 'location', 'rotation_euler', 'matrix_world', 'matrix_parent_inverse']
# Attributes that we want to import and export from lattice.data
data_lattice = ['points_u', 'points_v', 'points_w', 'interpolation_type_u', 'interpolation_type_v', 'interpolation_type_w', 'use_outside', 'vertex_group']

from_deformation_cage = ['hide_render', 'hide_viewport', 'hide_select', 'matrix_local', 'name', 'parent', 'type', 'name', 'show_wire', 'show_all_edges', 'display_type']

shape_keys_ppties = ['vertex_group', 'value', 'slider_min', 'slider_max', 'name', 'mute', 'interpolation', 'frame', 'relative_key']


# Function abble to export all the modifiers from an object
def export_modifiers(obj, restrict=[]):

    modifiers = []
    for m in obj.modifiers:
        modifiers.append(extract_variables(m, restrict))
        modifiers[-1]['type'] = getattr(m, 'type')
    
    return modifiers


# Function abble to export the vertex groups from an object
def export_vertex_groups(obj):
    v_groups = {}
    vertex_groups = [g.name for g in obj.vertex_groups]
    points_groups = [[] for g in vertex_groups]
    
    vertices = obj.data.vertices if obj.type == 'MESH' else obj.data.points

    for index, vertex in enumerate(vertices):
        groups = [group.group for group in vertex.groups]
        for group in groups:
            points_groups[group].append((index, obj.vertex_groups[group].weight(index)))
            
    v_groups['group_names'] = vertex_groups
    v_groups['groups'] = points_groups

    return v_groups


def get_target_lattice(obj):
    hook = None
    for modif in obj.modifiers:
        if modif.type == 'HOOK':
            hook = modif
            break
    if hook is None:
        return None
    elif hook.subtarget is None:
        return None
    else:
        return hook.subtarget


def export_drivers_general(obj, sk=[]):
    drivers = {}

    if obj is None:
        return {}

    if obj.animation_data is not None:
        for driver in obj.animation_data.drivers:
            if ("modifiers[" in driver.data_path) or one_of(sk, driver.data_path):
                drivers[driver.data_path] = {'data': export_driver(driver.driver), 'array_index': driver.array_index}

    return drivers


# Function abble to export a lattice and a way to rebuild it
def export_lattice(obj, differential):

    lattice = {}
    lattice['basis'] = extract_variables(obj, from_lattice)
    lattice['data'] = extract_variables(obj.data, data_lattice)
    
    # Exporting all vertex groups
    lattice['vertex_groups'] = export_vertex_groups(obj)

    lattice['modifiers'] = export_modifiers(obj)

    lattice['drivers'] = export_drivers_general(obj)

    return lattice


def export_mesh(obj):
    # Exporting geometry using obj standard (but in a JSON)
    vertices = [tuple(v.co) for v in obj.data.vertices]
    edges    = [tuple(e.vertices) for e in obj.data.edges]
    faces    = [tuple(p.vertices) for p in obj.data.polygons]

    return {'vertices': vertices, 'edges': edges, 'faces': faces}

# Function abble to export a deformation cage and a way rebuild it
def export_deform_cage(obj, differential):
    
    cage = {}

    # Exporting pure geometry (mesh):
    cage['geometry'] = export_mesh(obj)

    # Exporting basic attributes of the deformation cage:
    cage['basis'] = extract_variables(obj, from_deformation_cage)
    
    # Exporting all vertex groups:
    cage['vertex_groups'] = export_vertex_groups(obj)

    # Exporting modifiers:
    cage['modifiers'] = export_modifiers(obj)
    
    # Exporting shape keys:
    shape_keys = {}
    sk_found = []

    if obj.data.shape_keys is not None:
        shape_keys['basis'] = {'use_relative': obj.data.shape_keys.use_relative}
        shape_keys['key_blocks'] = []

        for shape_key in obj.data.shape_keys.key_blocks:
            shape_key_data = {}
            sk_found.append(shape_key.name)
            shape_key_data['basis'] = extract_variables(shape_key, shape_keys_ppties)
            shape_key_data['data'] = []

            for vertex in shape_key.data:
                shape_key_data['data'].append(tuple(vertex.co))
            
            shape_keys['key_blocks'].append(shape_key_data)
        

    cage['shape_keys'] = shape_keys

    # Exporting drivers:
    cage['drivers'] = {}
    cage['drivers']['general'] = export_drivers_general(obj)
    cage['drivers']['shape_keys'] = export_drivers_general(obj.data.shape_keys, sk_found)
    
    return cage


# Function that exports deformers
def export_deformers(modules, differential):
    # Trying to find the _deform collection, ending if not found
    deform_collection = locate_deform_collection()

    if deform_collection is None:
        return []

    exported_deformers = []
    
    # Checking if a deformer is usefull for a module
    for deformer in deform_collection.objects:
        
        if deformer.type == 'LATTICE':
            target = get_target_lattice(deformer)
            if target is None:
                continue

            for group_name in modules.keys():
                if 'deformers' not in modules[group_name]:
                    modules[group_name]['deformers'] = []

                data = modules[group_name]

                if target in data['bones']:
                    modules[group_name]['deformers'].append(deformer.name)

            exported_deformers.append(export_lattice(deformer, differential))


        elif deformer.type == 'MESH':
            # Checking if the deformation cage is used by some modules
            v_groups = [g.name for g in deformer.vertex_groups]

            for group_name in modules.keys():
                if 'deformers' not in modules[group_name]:
                    modules[group_name]['deformers'] = []

                data = modules[group_name]
                # For each bone of the module, does it appear in the vertex groups names
                bone_in_v_groups = any([(bone in v_groups) for bone in data['bones']])

                if bone_in_v_groups:
                    modules[group_name]['deformers'].append(deformer.name)

            exported_deformers.append(export_deform_cage(deformer, differential))

        else:
            print("Skip deformer '{}', unknown type".format(deformer.name))

    return exported_deformers
