bl_info = {
    "name": "Rig Transfer",
    "description": "Manager of rigs for updates",
    "author": "Les Fées Spéciales (LFS)",
    "version": (1, 0),
    "blender": (2, 91),
    "location": "Properties > Object > Rig Transfer",
    "warning": "",
    "category": "Rigging"
}

import bpy
from bpy.app.handlers import persistent

from .settings import RigTransferImportedItems, RigTransferSettings, DispAndPathProperty, SearchingChunk
from .operator import AutoNameExportRig, RigExportOperator, RigImportOperator, ApplyImportedModules, TestOperator, RenameArmatureOperator, ChooseDirectoryOperator, RemoveModuleOperator, ProbeModulesOperator, TransferFoundModuleOperator, GoClosestDirOperator
from .panel import RigTransferPanel


classes = [
    DispAndPathProperty,
    SearchingChunk,
    TestOperator,
    RigTransferImportedItems,
    RigTransferSettings,
    AutoNameExportRig,
    RigExportOperator,
    RigImportOperator,
    ApplyImportedModules,
    RenameArmatureOperator,
    ChooseDirectoryOperator,
    RemoveModuleOperator,
    ProbeModulesOperator,
    TransferFoundModuleOperator,
    GoClosestDirOperator,
    RigTransferPanel,
]

@persistent
def clear_data_handler(dummy):
    bpy.context.scene.rig_transfer.probed_data.clear()

def register():
    for c in classes:
        bpy.utils.register_class(c)

    bpy.app.handlers.load_post.append(clear_data_handler)

    bpy.types.Scene.rig_transfer = bpy.props.PointerProperty(type=RigTransferSettings)


def unregister():
    bpy.app.handlers.load_post.remove(clear_data_handler)
    
    clss = classes.copy()
    clss.reverse()
    for c in clss:
        bpy.utils.unregister_class(c)