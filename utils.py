import bpy
from .default_rig import auto_rig_default

ENV = {
    'version': 4.1,
    'groups': {}
}

# Build bones hierarchy from a given an armature
def tree_from_collection(armature):
    """
    armature: Source armature
    """

    tree = {}
    
    try:
        bones = armature.pose.bones
    except:
        return False, {}

    # 'bones' is a 1D list that doesn't consider hierarchy of bones
    for bone in bones:
        tree[bone.name] = {'children': [b.name for b in bone.children]}

    # Dual chaining must be done in another loop to be sure that everything is instanciated
    for bone, data in tree.items():
        for bone_name in data['children']:
            # Bone that never appear as child of any other bones don't have parent property
            tree[bone_name]['parent'] = bone

    # flat_list contains all bones that appear as child of a bone
    flat_list = [item for sublist in tree.values() for item in sublist['children']]

    data_hierarchy = {
        'tree': tree,
        'tree_roots': [item.name for item in bones if item.name not in flat_list]
    }

    return True, data_hierarchy


# List of bones that don't part of the default Siren rig
def differential_bones(tree):
    return set(tree.keys()).difference(auto_rig_default)


# Check if the active object is the main armature, according naming conventions
def is_main_armature_active(context):
    try:
        asset_name = context.scene.collection.children[0].name
    except:
        return False

    expected_name = asset_name + "_rig"
    obj = context.object

    # Check that the active object is the main armature
    if obj is None:
        return False
    else:
        if (obj.type != 'ARMATURE') or (obj.name != expected_name):
            return False
        else:
            return True