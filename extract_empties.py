import bpy
from .extract_constraints import extract_variables

# 'matrix_parent_inverse', 'matrix_basis', 'matrix_local', 'matrix_world'
from_empty = ['location', 'scale', 'rotation_euler', 'parent', 'parent_type', 'empty_display_type', 'empty_display_size', 'track_axis', 'up_axis', 'name', 'matrix_parent_inverse']

# Locate and return the "asset_name_subrig" collection

def subrig_collection():
    colls = [c for c in bpy.data.collections if c.name.endswith('_subrig')]

    if len(colls) > 0:
        return colls[0]
    else:
        return None


def vertices_position(empty):
    parent = empty.parent
    if parent is None:
        return []

    indices = list(empty.parent_vertices)
    vertices = [{'index': i, 'co': tuple(parent.data.vertices[i].co)} for i in indices] # Fetching locations to check on the other side
    return vertices


def extract_empties(modules, differential):
    subrig = subrig_collection()
    if subrig is None:
        return []

    empties = set([o for o in subrig.objects if (o.type == 'EMPTY')])
    targets = {}

    for bone in bpy.context.object.pose.bones:
        if (bone.name not in differential) or (bone.bone_group is None) or (not bone.bone_group.name.endswith(".module")):
            continue

        for c in bone.constraints:
            if 'target' in dir(c):
                if c.target is not None:
                    targets[c.target] = bone.bone_group.name # Keeping what module this empty is useful in

    kept_targets = set(targets.keys())
    exported = empties.intersection(kept_targets) # Empty that appears as target of a constraint on a differential bone

    empties_data = []
    for empty in exported:
        empty_data = {}
        empty_data['module'] = targets[empty]
        empty_data['data'] = extract_variables(empty, from_empty)
        empty_data['positions'] = vertices_position(empty)
        empties_data.append(empty_data)

    return empties_data
