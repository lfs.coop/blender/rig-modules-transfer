import bpy
import json
import os

from .utils import tree_from_collection, differential_bones, ENV
from .default_rig import auto_rig_default
from .extract_constraints import extract_constraints, export_drivers, extract_variables
from .extract_deformers import export_deformers
from .WarningCollector import WARNINGS_COLLECTOR
from .manage_cs import export_custom_shapes
from .extract_empties import extract_empties
from .extract_curves import extract_curves

from_bone = ['inherit_scale', 'bbone_segments', 'bbone_x', 'bbone_z', 'use_endroll_as_inroll', 'bbone_handle_type_start', 'bbone_custom_handle_start', 'bbone_handle_type_end', 'bbone_custom_handle_end', 'use_deform', 'envelope_distance', 'envelope_weight', 'use_envelope_multiply', 'head_radius', 'tail_radius']
from_pose_bone = ['rotation_mode', 'bbone_curveinx', 'bbone_curveiny', 'bbone_curveoutx', 'bbone_curveouty', 'bbone_rollin', 'bbone_rollout', 'bbone_scaleinx', 'bbone_scaleiny', 'bbone_scaleoutx', 'bbone_scaleouty', 'bbone_easein', 'bbone_easeout', 'ik_stretch', 'lock_ik_x', 'lock_ik_y', 'lock_ik_z', 'ik_stiffness_x', 'ik_stiffness_y', 'ik_stiffness_z', 'use_ik_limit_x', 'ik_min_x', 'ik_max_x', 'use_ik_limit_y', 'ik_min_y', 'ik_max_y', 'use_ik_limit_z', 'ik_min_z', 'ik_max_z']

# Checks that modules won't break when they will arrive in another file (according some tree rules)
def check_modules_validity(modules, differential):
    
    # For each module and the bones that it contains
    for module_name, module_data in modules.items():
        contains_diff = False
        # For each bone of the module
        for bone_name in module_data['bones']:
            # If the bone is not from the Auto Rig Pro
            if bone_name not in differential:
                WARNINGS_COLLECTOR.add_warning("Modules errors", "Bone '{}' from module '{}' is from Auto Rig Pro".format(bone_name, module_name))
                continue
            else:
               contains_diff = True 

            bone = bpy.context.object.pose.bones.get(bone_name)
            sequence = []
            
            # Climb back the hierarchy
            while (bone is not None) and (bone.name in module_data['bones']):
                sequence.append(bone.name)
                bone = bone.parent

            sequence.reverse()
            if (bone is not None):
                if bone.name in differential:
                    # ERROR. It's impossible to reach the armature with this module
                    # This module is potentially impossible to import inside a hierarchy, it will float in the void
                    WARNINGS_COLLECTOR.add_warning("Modules errors", "Module {} is not self-sufficient: {}".format(module_name, " > ".join(sequence)))
                else:
                    # OK. The bone is from the Auto Rig Pro, it will be the anchor of that module
                    pass
            else:
                # OK. The root of the hierarchy was reached.
                pass

        if not contains_diff:
            WARNINGS_COLLECTOR.add_warning("Empty module", "Module '{}' is empty or only contains bones from Auto Rig Pro".format(module_name))

# Checks that no differential bone is out of all modules
def check_diff_bones_in_modules(modules, differential):
    heap = []
    for name, data in modules.items():
        heap.extend(data['bones'])
    heap = set(heap) # Removing duplicates

    for bone in differential:
        if bone not in heap:
            WARNINGS_COLLECTOR.add_warning("Bones out of all modules", "Differential bone '{}' is not part of any module".format(bone))

    if len(modules) == 0:
        WARNINGS_COLLECTOR.add_warning("No module found", "No module was found while exporting")

# Get all the basic data about armature and call function to aggregate more complex data
def gather_data(armature, hierarchy_data, differential):
    tree = hierarchy_data['tree']
    for bone_name, data in tree.items():
        # Is differential ? (if it's not, only hierarchy data is required)
        if bone_name in differential:
            data['differential'] = True

            bpy.ops.object.mode_set(mode='EDIT')
            edit_bone = armature.data.edit_bones[bone_name]

            if edit_bone is None:
                WARNINGS_COLLECTOR.add_warning("Data gathering errors", "Data can't be exported for bone {}".format(bone_name))
                continue

            # Transformations (Head, Tail, Radius, Roll, Length, Envelope)
            data['transformations'] = {
                'head': tuple(edit_bone.head),
                'tail': tuple(edit_bone.tail),
                'matrix': [tuple(o) for o in list(edit_bone.matrix)],
                'head_radius': edit_bone.head_radius,
                'tail_radius': edit_bone.tail_radius,
                'length': edit_bone.length,
                'envelope_distance': edit_bone.envelope_distance,
            }

            pose_bone = armature.pose.bones.get(bone_name)

            if pose_bone is None:
                continue

            bone = pose_bone.bone

            # Custom Shape
            if pose_bone.custom_shape is not None:
                if not pose_bone.custom_shape.name.startswith('cs_'):
                    WARNINGS_COLLECTOR.add_warning("Name of custom shape changed", "Name '{}' will be fixed".format(pose_bone.custom_shape.name))
                    pose_bone.custom_shape.name = "cs_" + pose_bone.custom_shape.name.lower().replace('.', '_')
                data['custom_shape'] = {'cs': pose_bone.custom_shape.name, 'scale': pose_bone.custom_shape_scale, 'transform': pose_bone.custom_shape_transform.name if pose_bone.custom_shape_transform is not None else "", 'scale_bone_length': pose_bone.use_custom_shape_bone_size, 'wireframe': bone.show_wire}

            # Visibility layers
            data['layers'] = list(bone.layers)

            # Custom properties
            custom_properties = dict(pose_bone.items())
            try:
                custom_properties.pop('_RNA_UI')
            except:
                pass

            if len(custom_properties) > 0:
                data['custom_properties'] = custom_properties

            # Secondary properties, with no order of affectation
            data['side_properties_pose'] = extract_variables(pose_bone, from_pose_bone)
            data['side_properties_arma'] = extract_variables(bone, from_bone)

            # Bones Constraints
            data['constraints'] = extract_constraints(pose_bone)

        else:
            data['differential'] = False

    # Drivers on constraints
    if armature.animation_data is not None:
        hierarchy_data['drivers'] = export_drivers(armature.animation_data.drivers, differential)
    else:
        hierarchy_data['drivers'] = {}

    bpy.ops.object.mode_set(mode='OBJECT')

    # Meta data
    hierarchy_data['version_exporter'] = ENV['version']

# Check if there are modules to import in that file
def identify_modules(armature):
    modules = {}
    for bone in armature.pose.bones:
        group = bone.bone_group
        if (group is not None):
            if(group.name.split('.')[-1].lower() == "module"):
                #modules.setdefault(group.name, {'bones': [], 'color': group.color_set if group.color_set != 'CUSTOM' else 'DEFAULT'})
                modules.setdefault(group.name, {'bones': [], 'color': group.color_set})
                if group.color_set == 'CUSTOM':
                    modules[group.name]['colors'] = {
                        'normal': tuple(group.colors.normal),
                        'select': tuple(group.colors.select),
                        'active': tuple(group.colors.active)
                    }
                modules[group.name]['bones'].append(bone.name)
    return modules

# Export the formatted JSON to a file on the disk
def export_to_file(to_export):
    full_path = bpy.path.abspath(bpy.context.scene.rig_transfer.path_dir_export)

    asset_name = bpy.context.scene.collection.children[0].name
    generic = bpy.context.scene.rig_transfer.generic_name

    to_write = json.dumps(to_export)
    if generic:
        to_write = to_write.replace(asset_name, '<asset_name>')

    full_path = os.path.join(full_path, bpy.context.scene.rig_transfer.export_file_name.replace('/', '').replace('\\', ''))
    print("[Rig Transfer]", "Exporting JSON to '{}'".format(full_path))
    file = open(full_path, 'w')
    file.write(to_write)
    file.close() 


def rig_exporter(armature):
    status, hierarchy_data = tree_from_collection(armature)

    if status:
        differential = differential_bones(hierarchy_data['tree'])
        gather_data(armature, hierarchy_data, differential)
        modules = identify_modules(armature)

        check_modules_validity(modules, differential)
        check_diff_bones_in_modules(modules, differential)
        hierarchy_data['modules'] = modules
        hierarchy_data['deformers'] = export_deformers(modules, differential)
        hierarchy_data['custom_shapes'] = export_custom_shapes()
        hierarchy_data['empties'] = extract_empties(modules, differential)
        hierarchy_data['curves'] = extract_curves(modules, differential)
        export_to_file(hierarchy_data)
        