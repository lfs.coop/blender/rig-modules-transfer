import bpy
from .WarningCollector import WARNINGS_COLLECTOR

# Attributes common to all blender object (rna)
common       = set(['show_expanded', '__doc__', '__slots__', 'error_rotation', 'is_proxy_local', 'type', 'is_valid', 'rna_type', '__module__', 'error_location', 'bl_rna', 'id_type'])
prop_types   = set(['bpy_prop_collection', 'bpy_prop_array'])
basic_types  = set(['int', 'bool', 'float', 'str'])
object_source_types = set(['Matrix', 'Vector', 'Action', 'Object', 'Euler'])
all_types    = prop_types.union(basic_types).union(object_source_types)

def extract_variables(object_source, base=[]):
    # We keep only attributes specific to that object_source, not attributes common to all Python object_sources or Blender object_sources
    to_add = [o for o in dir(object_source) if o not in common] if len(base) == 0 else base
    report = {}

    # Attributes need to be formatted to be serializable in JSON
    for attribute_name in to_add:
        attribute = getattr(object_source, attribute_name)

        if attribute is not None:
            type_obj = type(attribute).__name__
            if type_obj in all_types:
                if type_obj in basic_types:
                    report[attribute_name] = attribute
                elif type_obj == 'Vector':
                    report[attribute_name] = (type_obj, tuple(attribute))
                elif type_obj == 'Matrix':
                    mtr = tuple([tuple(o) for o in list(attribute)])
                    report[attribute_name] = (type_obj, mtr)
                elif type_obj == 'Action':
                    report[attribute_name] = (type_obj, attribute.name)
                elif type_obj == 'Euler':
                    report[attribute_name] = (type_obj, (tuple(attribute), attribute.order))
                elif type_obj == 'Object':
                    report[attribute_name] = (type_obj, attribute.name)
                elif type_obj == 'bpy_prop_collection':
                    report[attribute_name] = (type_obj, [extract_variables(sub_object_source) for sub_object_source in attribute])
                elif (type_obj == 'bpy_prop_array') and (len(attribute) > 0) and (type(attribute[0]).__name__ in basic_types):
                    report[attribute_name] = (type_obj, [sub_object_source for sub_object_source in attribute])
                else:
                    WARNINGS_COLLECTOR.add_warning("Unknown type", "Unknown type {}".format(type_obj))
            else:
                try:
                    WARNINGS_COLLECTOR.add_warning("Unknown attribute", "Impossible to save property '{}' from '{}'".format(attribute_name, object_source.name))
                except:
                    WARNINGS_COLLECTOR.add_warning("Unknown attribute", "Impossible to save property '{}' from an unnamed container".format(attribute_name))
                
    return report


def extract_constraints(bone):
    constraints = {}
    for c in bone.constraints:
        if c.is_valid:
            constraint = {'type': c.type}
            constraint.update(extract_variables(c))
            constraints[c.name] = constraint
        else:
            WARNINGS_COLLECTOR.add_warning("Invalid constraints", "Constraint {} from bone {} is not valid".format(c.name, bone.name))
    return constraints


def export_driver(driver_data):
    driver = {}
    driver['type'] = driver_data.type
    driver['expression'] = driver_data.expression
    driver['use_self'] = driver_data.use_self

    variables = []
    for variable_data in driver_data.variables:
        targets = []
        variable = {}
        variable['name'] = variable_data.name
        variable['type'] = variable_data.type
        
        for target_data in variable_data.targets:
            target = {}
            target.update(extract_variables(target_data))
            targets.append(target)
            
        variable['targets'] = targets
        variables.append(variable)
            
    driver['variables'] = variables

    return driver

# Function that checks if at least one of the keys is present in 'name'
def one_of(list_bones, data_path):
    target = set(data_path.split('"'))
    potentials = set(list_bones)
    return len(target.intersection(potentials)) > 0


def export_drivers(drivers_data, differential):
    drivers = {}

    if drivers_data is None:
        return {}
    for driver in drivers_data:
        if driver.is_valid:
            # If the driver is on a differential bone
            if one_of(differential, driver.data_path): # If the driver is about a differential bone
                drivers[driver.data_path] = {'data': export_driver(driver.driver), 'array_index': driver.array_index}
        else:
            WARNINGS_COLLECTOR.add_warning("Invalid drivers", "The driver: \"{}\" is not valid".format(driver.data_path))
    return drivers