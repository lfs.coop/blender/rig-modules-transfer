import bpy

from .rig_importer import get_imported_data

def remove_module(module_name):
	data = get_imported_data()
	modules = data['modules']
	bpy.ops.object.mode_set(mode='EDIT')

	for bone_name in modules[module_name]['bones']:
		bone = bpy.context.object.data.edit_bones.get(bone_name)
		if bone is not None:
			bpy.context.object.data.edit_bones.remove(bone)

	bpy.ops.object.mode_set(mode='OBJECT')

	if 'deformers' in modules[module_name]:
		for deformer_name in modules[module_name]['deformers']:
			deformer = bpy.data.objects.get(deformer_name)
			if deformer is not None:
				bpy.data.objects.remove(deformer)

	b_g = bpy.context.object.pose.bone_groups.get(module_name)
	if b_g is not None:
		bpy.context.object.pose.bone_groups.remove(b_g)