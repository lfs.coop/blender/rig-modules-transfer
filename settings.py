import bpy
from pathlib import Path

sources = [
    ('ASSET', "Other Asset", "Import rig or modules from another .blend file", 'FILE_BLEND', 0),
    ('JSON', "JSON File", "Import rig or modules from a template JSON file", 'FILE_TICK', 1)
]

operations = [
    ('EXPORT', "Export Armature", "Export active armature and modules to a JSON file", 'EXPORT', 0),
    ('IMPORT', "Import Armature", "Import rig or modules", 'IMPORT', 1)
]


def check_valid_file(self, context):
    # Starts by reseting the previous session (if it exists)
    bpy.context.scene.rig_transfer.probed_data.clear()
    
    path = context.scene.rig_transfer.file_path
    file = Path(bpy.path.abspath(path))

    ext = "json" if context.scene.rig_transfer.enum_sources == 'JSON' else "blend"

    # Then, checks that the extension is .json and that it exists on the disk
    if file.exists() and file.is_file():
        found = path.split('.')[-1].lower() # Extension
        context.scene.rig_transfer.valid_file = (found == ext)
    else:
        context.scene.rig_transfer.valid_file = False

# Checks that the provided directory exists on the disk
def check_valid_dir(self, context):
    path = context.scene.rig_transfer.path_dir_export
    file = Path(bpy.path.abspath(path))

    if file.exists() and file.is_dir():
        context.scene.rig_transfer.valid_export_dir = True
    else:
        context.scene.rig_transfer.valid_export_dir = False

# Check that the exporting name is valid (no space, .json, ...)
def check_valid_name(self, context):
    name = self.export_file_name
    lower = name.lower()

    if lower != name:
        self.export_file_name = lower

    if lower.split('.')[-1] != "json":
        self.export_file_name = lower + ".json"

    if lower == ".json":
        self.export_file_name = context.scene.collection.children[0].name + "_rig_export.json"

    context.scene.rig_transfer.valid_export_file = True

# Function that locks other checkboxes when 'ALL' is checked
def update_function(self, context):
    if self.name_module == 'ALL':
        pass
    else:
        all = None
        for item in bpy.context.scene.rig_transfer.probed_data:
            if item.name_module == "ALL":
                all = item
                break

        if all.imported and (self.imported != self.old):
            self.imported = self.old

# Structures containing all modules found in the provided JSON file
class RigTransferImportedItems(bpy.types.PropertyGroup):
    name_module: bpy.props.StringProperty()
    imported: bpy.props.BoolProperty(update=update_function)
    old: bpy.props.BoolProperty()

    def set(self, name, value):
        self.name_module = name
        self.old = self.imported
        self.imported = value


class DispAndPathProperty(bpy.types.PropertyGroup):
    
    name: bpy.props.StringProperty()
    full_path: bpy.props.StringProperty()


class SearchingChunk(bpy.types.PropertyGroup):
    look_for: bpy.props.StringProperty(name="Modules source", default='')
    look_in: bpy.props.CollectionProperty(type=DispAndPathProperty)


# All the data required for the addon
class RigTransferSettings(bpy.types.PropertyGroup):
    # For import of modules
    valid_file: bpy.props.BoolProperty(default=False)
    file_path: bpy.props.StringProperty(name="Path", default="", subtype="FILE_PATH", update=check_valid_file)

    # For export of modules
    valid_export_file: bpy.props.BoolProperty(default=False)
    export_file_name: bpy.props.StringProperty(name="File Name", default="", update=check_valid_name)

    valid_export_dir: bpy.props.BoolProperty(default=True)
    path_dir_export: bpy.props.StringProperty(name="Directory Path", default="//", subtype="DIR_PATH", update=check_valid_dir)

    # From where should we import ?
    enum_sources: bpy.props.EnumProperty(items=sources, name="Source", default="JSON", update=check_valid_file)
    enum_operations: bpy.props.EnumProperty(items=operations, name="Operation", default="EXPORT")

    # What's been found
    probed_data: bpy.props.CollectionProperty(type=RigTransferImportedItems)

    all_differentials: bpy.props.BoolProperty(name="Import All Differentials", default=False)
    generic_name: bpy.props.BoolProperty(name="Use Generic Name", default=True)

    pool_probe_modules: bpy.props.PointerProperty(type=SearchingChunk)
        