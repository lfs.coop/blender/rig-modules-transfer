import bpy
from .extract_deformers import export_mesh
from .WarningCollector import WARNINGS_COLLECTOR

def fetch_cs_collection():
    try:
        cs_collection = [cs for cs in bpy.data.collections if cs.name.endswith('_cs')][0]
    except:
        cs_collection = None

    if cs_collection is None:
        cs_collection = bpy.data.collections.new(bpy.context.scene.collection.children[0].name + "_cs")
        bpy.context.scene.collection.children.link(cs_collection)

    return cs_collection


def export_custom_shapes():
    custom_shapes = {}

    for obj in bpy.data.objects:
        if obj.name.startswith('cs_'):
            custom_shapes[obj.name] = export_mesh(obj)

    return custom_shapes


def import_custom_shape(name_cs, cs_data, cs_collection):
    geometry_cs = bpy.data.meshes.new(name_cs)
    geometry_cs.from_pydata(cs_data[name_cs]['vertices'], cs_data[name_cs]['edges'], cs_data[name_cs]['faces'])

    object_cs = bpy.data.objects.new(name_cs, geometry_cs)
    cs_collection.objects.link(object_cs)

    WARNINGS_COLLECTOR.add_warning("Import of custom shape(s)", "Custom shape '{}' was imported".format(name_cs))

    return object_cs