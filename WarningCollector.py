import bpy
import os

class WarningsCollector():
    
    warnings = {}

    def reset(self):
        self.warnings.clear()

    def add_category(self, category):
        self.warnings.setdefault(category.lower().capitalize(), set())

    def add_warning(self, category, error):
        self.add_category(category)
        self.warnings[category.lower().capitalize()].add(error)

    def add_warnings(self, category, errors):
        self.add_category(category)
        errors = set(errors)
        self.warnings[category.lower().capitalize()] = self.warnings[category.lower().capitalize()].union(errors)

    def export_warnings(self, name):
        path = os.path.join(bpy.path.abspath("//"), name.replace('\\', '').replace('/', '')) #"/warnings_module_exporter.txt"
        print("[Rig Transfer]", "Exporting report to '{}'".format(path))
        file = open(path, 'w')

        for category, warns in self.warnings.items():
            file.write("###################################################################################\r")
            file.write("#                                                                                 #\r")
            file.write("#           " + category + " " * (70-len(category)) + "#\r")
            file.write("#                                                                                 #\r")
            file.write("###################################################################################\r\r")
            warns = set(warns)
            for idx, w in enumerate(warns):
                index = f'{idx:03}'
                file.write("[ ] {}. ".format(index) + w + "\r")

            file.write("\r\r\r")


WARNINGS_COLLECTOR = WarningsCollector()