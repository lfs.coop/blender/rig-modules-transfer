import bpy
import os
import json

#PATH = "/home/clement.benedetti/Bureau/lib/chars/utils/rigmod/Pari/v003/pari_rig_export.json"
#PATH = "/u/sirene/lib/chars/main/ali/rigging/rig_ok_blend/v006/lib_chars_main_ali_rig_rig_ok.blend"
PATH = "//"
    

def try_detect_folder():
    path = bpy.path.abspath(PATH)
    path = path.replace('\\', '/')

    if '/lib/chars/' in path:
        splitted = path.split('/lib/chars/')
        path = splitted[0]  + '/lib/chars/utils/rigmod/'
        return path
    else:
        return None

def reverse(l):
    l.reverse()
    return l

def probe_modules(path):
    dirs = os.listdir(path)
    main_path = path
    paths_modules = []
    
    for dir in dirs:
        
        # For each character that has been used as modules source
        path_dir = os.path.join(path, dir)

        if (not os.path.isdir(path_dir)) or (not dir.startswith('lib_chars_')):
            continue
        
        # At this level of the hierarchy, a directory == a name of character (in theory)
        asset_name = ""
        for chunk in reverse(dir.split('_')):
            if chunk != 'rigmod':
                asset_name = chunk + asset_name
            else:
                break

        versions = os.listdir(path_dir)
        versions.sort()

        # We take the last version, and we make sure that it's not current
        if (len(versions) == 0) or (not versions[-1].startswith('v')) or (not versions[-1][1:].isdigit()):
            continue
        path_last_version = os.path.join(path_dir, versions[-1])

        # We check if a JSON is present in there
        try:
            json_path = [os.path.join(path_last_version, f) for f in os.listdir(path_last_version) if os.path.isfile(os.path.join(path_last_version, f)) and f.lower().endswith('.json')][0]
        except:
            continue

        file = open(json_path, 'r')
        content = json.loads(file.read())
        file.close()
        modules = content['modules']
        
        # We create pairs of (full_name, name_to_display)
        #for name in modules.keys():
        paths_modules.append((json_path, asset_name.capitalize() + ': ' + ", ".join([name.replace('.module', '').capitalize() for name in modules.keys()])))
        
        # JSONs are pretty big, so we don't wait for the garbage collector to destroy them
        del content, modules
        
    return paths_modules
        
def fill_searching_bar(paths_modules):
    bpy.context.scene.rig_transfer.pool_probe_modules.look_in.clear()
    for full_name, display in paths_modules:
        nouv = bpy.context.scene.rig_transfer.pool_probe_modules.look_in.add()
        nouv.name = display
        nouv.full_path = full_name




