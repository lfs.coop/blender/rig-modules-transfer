import bpy
from mathutils import Vector, Matrix, Euler
from collections import deque
from .WarningCollector import WARNINGS_COLLECTOR

to_skip = ['type', 'frame', 'is_bind']

def unpack_data(constraint, data_constraint, ignore=[]):
    
    to_set = deque(data_constraint.keys())

    seen = set()

    while len(to_set) > 0:
        
        key = to_set.popleft()
        item = data_constraint[key]
    
        if (key in to_skip) or (key in ignore):
            continue

        # Complex type, will require conversion from JSON data
        if type(item) is list:
            type_data, raw_data = item
            converted_data = None

            if type_data == 'Vector':
                converted_data = Vector(raw_data)
            elif type_data == 'Matrix':
                converted_data = Matrix(raw_data)
            elif type_data == 'Action':
                converted_data = bpy.data.actions.get(raw_data)
            elif type_data == 'Object':
                converted_data = bpy.data.objects.get(raw_data)
            elif type_data == 'Euler':
                converted_data = Euler(tuple(raw_data[0]), raw_data[1])
            elif type_data == 'bpy_prop_collection':
                for data_block in raw_data:
                    target = getattr(constraint, key).new()
                    unpack_data(target, data_block)
            elif type_data == 'bpy_prop_array':
                methods = dir(constraint)
                desired = key + "_set"
                if (desired not in methods) or (len(raw_data) == 0) or (type(raw_data[0]) is list):
                    continue
                else:
                    try:
                        getattr(constraint, desired)(raw_data)
                    except:
                        pass

            
            if converted_data is not None:
                try:
                    setattr(constraint, key, converted_data)
                except:
                    if key not in seen:
                        to_set.append(key)
                    else:
                        WARNINGS_COLLECTOR.add_warning("Impossible to set property", "Failed to set property '{}' of object {} to '{}'".format(key, constraint, raw_data))
            else:
                WARNINGS_COLLECTOR.add_warning("Impossible to set property", "Failed to set property '{}' of object {} to '{}'".format(key, constraint, raw_data))

        # Python native type
        else:
            try:
                setattr(constraint, key, item)
            except:
                if key not in seen:
                    to_set.append(key)
                else:
                    WARNINGS_COLLECTOR.add_warning("Impossible to set property", "Failed to set property '{}' of object {} to '{}'".format(key, constraint, item))

        seen.add(key)

def clear_set_inverse(constraints_imported):
    if 'CHILD_OF' in constraints_imported:
        for bone, constraint in constraints_imported['CHILD_OF']:
            bpy.context.object.data.bones.active = bone.bone
            context_py = bpy.context.copy()
            context_py["constraint"] = constraint
            context_py["active_pose_bone"] = bone
            context_py["bone"] = bone.bone
            try:
                bpy.ops.constraint.childof_clear_inverse(context_py, constraint=constraint.name, owner='BONE')
                bpy.ops.constraint.childof_set_inverse(context_py, constraint=constraint.name, owner='BONE')
            except:
                WARNINGS_COLLECTOR.add_warning("Postprocess", "Failed to reset inverse on '{}' from '{}'".format(constraint.name, bone.name))


def postprocess(constraints_imported):
    bpy.ops.object.mode_set(mode='POSE')
    clear_set_inverse(constraints_imported)
    bpy.ops.object.mode_set(mode='OBJECT')


def import_constraints(bone, bone_constraints, constraints_imported):
    for name_constraint, data_constraint in bone_constraints.items():
        constraint = bone.constraints.new(type=data_constraint['type'])
        constraint.name = name_constraint
        unpack_data(constraint, data_constraint)

        constraints_imported.setdefault(data_constraint['type'], [])
        constraints_imported[data_constraint['type']].append((bone, constraint))


def constraint_from_datapath(armature, data_path):
    splitted = data_path.split('.')
    attribute = splitted[-1]
    expr = "armature." + ".".join(splitted[:-1])
    
    try:
        constraint = eval(expr) # Get the constraint if it exists
    except:
        WARNINGS_COLLECTOR.add_warning("Driver failed", "Driver {} could not be instanciated".format(expr))
        constraint = None

    return constraint, attribute


def rebuild_driver(constraint, attribute, driver_data):
    # Removing driver if one is instanciated by default
    constraint.driver_remove(attribute)
    # Re-adding a driver on the property
    driver = constraint.driver_add(attribute, driver_data['array_index'] if type(getattr(constraint, attribute)).__name__ in ['bpy_prop_array', 'Vector', 'Euler'] else -1)
    if type(driver) is list:
        for i in driver:
            WARNINGS_COLLECTOR.add_warning('Failed to instanciate driver', i.data_path)
    driver = driver.driver # Yeah, for real -> FCurve to driver
    # Basic informations about the driver
    data = driver_data['data']
    driver.type = data['type']
    driver.use_self = data['use_self']

    for variable_data in data['variables']:
        variable = driver.variables.new()
        variable.name = variable_data['name']
        variable.type = variable_data['type']
        for index, target_data in enumerate(variable_data['targets']):
            unpack_data(variable.targets[index], target_data)

    # Setting up the expression with all variables instanciated
    driver.expression = data['expression']