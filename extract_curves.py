import bpy
from .extract_constraints import extract_variables
from .extract_deformers import export_modifiers

# Must export Bezier and NURBS curves

from_curve   = ['display_type', 'empty_display_size', 'empty_display_type', 'location', 'name', 'rotation_axis_angle', 'rotation_euler', 'scale', 'show_axis', 'show_bounds', 'up_axis', 'matrix_parent_inverse']
from_c_data  = ['bevel_depth', 'bevel_factor_end', 'bevel_factor_mapping_end', 'dimensions', 'bevel_factor_mapping_start', 'bevel_factor_start', 'bevel_mode', 'bevel_object', 'bevel_profile', 'bevel_resolution', 'extrude', 'fill_mode', 'name', 'offset', 'render_resolution_u', 'render_resolution_v', 'resolution_u', 'resolution_v', 'taper_object', 'twist_mode', 'twist_smooth', 'use_fill_caps', 'use_fill_deform', 'use_map_taper', 'use_path', 'use_path_follow', 'use_radius', 'use_stretch']
from_nurbs   = ['hide', 'order_u', 'order_v', 'point_count_u', 'point_count_v', 'radius_interpolation', 'resolution_u', 'resolution_v', 'tilt_interpolation', 'use_cyclic_u', 'use_cyclic_v', 'use_endpoint_u', 'use_endpoint_v', 'use_smooth']
from_bezier  = ['hide', 'order_u', 'order_v', 'point_count_u', 'point_count_v', 'radius_interpolation', 'resolution_u', 'resolution_v', 'tilt_interpolation', 'use_bezier_u', 'use_bezier_v', 'use_cyclic_u', 'use_cyclic_v', 'use_endpoint_u', 'use_endpoint_v', 'use_smooth']

from_bezier_point = ['co', 'handle_left', 'handle_left_type', 'handle_right', 'handle_right_type', 'hide', 'radius', 'tilt', 'weight_softbody']
from_nurbs_point  = ['co', 'hide', 'radius', 'tilt', 'weight', 'weight_softbody']


def locate_deform_collection():
    candidates = [c for c in bpy.data.collections if c.name.endswith("_deform")]
    if len(candidates) > 0:
        return candidates[0]
    else:
        return None


def extract_nurbs(spline):
    basis = {}
    basis['basis'] = extract_variables(spline, from_nurbs)
    basis['geometry'] = [extract_variables(p, from_nurbs_point) for p in spline.points]

    return basis



def extract_bezier(spline):
    basis = {}
    basis['basis'] = extract_variables(spline, from_bezier)
    basis['geometry'] = [extract_variables(p, from_bezier_point) for p in spline.bezier_points]

    return basis



def extract_splines(curve):
    splines_data = []
    for spline in curve.data.splines:
        spline_data = {}
        if spline.type == 'NURBS':
            spline_data['type'] = 'NURBS'
            spline_data['data'] = extract_nurbs(spline)

        elif spline.type == 'BEZIER':
            spline_data['type'] = 'BEZIER'
            spline_data['data'] = extract_bezier(spline)

        splines_data.append(spline_data)

    return splines_data



def extract_curves(modules, differential):
    deform_collection = locate_deform_collection()

    if deform_collection is None:
        return []

    curves = set([c for c in deform_collection.all_objects if (c.type == 'CURVE')])
    targets = {}

    for bone in bpy.context.object.pose.bones:
        if (bone.name not in differential) or (bone.bone_group is None) or (not bone.bone_group.name.endswith(".module")):
            continue

        for c in bone.constraints:
            if 'target' in dir(c):
                if c.target is not None:
                    targets[c.target] = bone.bone_group.name # Keeping which module this curve is useful in

    kept_targets = set(targets.keys())
    exported = curves.intersection(kept_targets) # Curves that appears as target of a constraint on a differential bone

    curves_data = []

    for curve in exported:
        curve_data = {}
        curve_data['module'] = targets[curve]
        curve_data['basis'] = extract_variables(curve, from_curve)
        curve_data['data'] = extract_variables(curve.data, from_c_data)
        curve_data['splines'] = extract_splines(curve)
        curve_data['modifiers'] = export_modifiers(curve)
        curves_data.append(curve_data)

    return curves_data