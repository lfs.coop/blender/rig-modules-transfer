import bpy

from .import_constraints import unpack_data, rebuild_driver, constraint_from_datapath
from .WarningCollector import WARNINGS_COLLECTOR


def import_drivers(obj, drivers):
    for data_path, driver_data in drivers.items():
        driver_target, attribute_name = constraint_from_datapath(obj, data_path)
        if driver_target is not None:
            rebuild_driver(driver_target, attribute_name, driver_data)


def import_modifiers(obj, modifiers_data, ignore=[]):
    for modifier_block in modifiers_data:
        modifier = obj.modifiers.new(name=modifier_block['name'], type=modifier_block['type'])
        unpack_data(modifier, modifier_block, ignore)


def import_vertex_groups(obj, vertex_groups_data):
    for index_group, vertex_group_name in enumerate(vertex_groups_data['group_names']):
        if obj.vertex_groups.get(vertex_group_name) is None:
            vertex_group = obj.vertex_groups.new(name=vertex_group_name)
            for index_vertex, weight in vertex_groups_data['groups'][index_group]:
                vertex_group.add([index_vertex], weight, 'ADD')


def import_shape_keys(obj, shape_keys_data):

    if 'key_blocks' in shape_keys_data:
        for shape_key_block in shape_keys_data['key_blocks']:
            shape_key = obj.shape_key_add(name=shape_key_block['basis']['name'])
            unpack_data(shape_key, shape_key_block['basis'])

            for vertex, co in zip(shape_key.data, shape_key_block['data']):
                vertex.co = co

        obj.data.shape_keys.use_relative = shape_keys_data['basis']['use_relative']


def already_present(mesh_data):
    obj = bpy.data.objects.get(mesh_data['basis']['name'])

    if obj is not None:
        v1 = len(obj.data.vertices)
        v2 = len(mesh_data['geometry']['vertices'])

        if v1 == v2:
            return obj

        else:
            WARNINGS_COLLECTOR.add_warning("Conflict in deformers", "Conflict while importing deformer '{}'".format(mesh_data['basis']['name']))
            return None
    else:
        return None


def import_deform_cage(mesh_data, deform_collection):
    deformer_object = already_present(mesh_data)
    present = deformer_object is not None

    if not present:
        # Creating the geometry of the deformer
        deformer_mesh = bpy.data.meshes.new(mesh_data['basis']['name'])
        deformer_mesh.from_pydata(mesh_data['geometry']['vertices'], mesh_data['geometry']['edges'], mesh_data['geometry']['faces'])
        deformer_mesh.update()

        # Creating the object hosting the geometry
        deformer_object = bpy.data.objects.new(mesh_data['basis']['name'], deformer_mesh)

        deform_collection.objects.link(deformer_object)

        unpack_data(deformer_object, mesh_data['basis'])

    import_vertex_groups(deformer_object, mesh_data['vertex_groups'])

    if not present:
        import_modifiers(deformer_object, mesh_data['modifiers'])

        import_shape_keys(deformer_object, mesh_data['shape_keys'])

        import_drivers(deformer_object, mesh_data['drivers']['general'])

        if 'shape_keys' in mesh_data['drivers']:
            import_drivers(deformer_object.data.shape_keys, mesh_data['drivers']['shape_keys'])


def import_lattice(lattice_data, deform_collection):

    deform_object = bpy.data.objects.new(lattice_data['basis']['name'], bpy.data.lattices.new(lattice_data['basis']['name']))
    unpack_data(deform_object, lattice_data['basis'])
    deform_collection.objects.link(deform_object)

    unpack_data(deform_object.data, lattice_data['data'])

    # Importing vertex groups from the lattice
    import_vertex_groups(deform_object, lattice_data['vertex_groups'])

    # Importing modifiers on new lattice:
    import_modifiers(deform_object, lattice_data['modifiers'])

    import_drivers(deform_object, lattice_data['drivers'])



def locate_deform_collection(create=False):
    try:
        deform_collection = [c for c in bpy.data.collections if c.name.endswith('_def') or c.name.endswith('_deform')][0]
    except:
        deform_collection = None

    if not create:
        return deform_collection

    else:
        if deform_collection is not None:
            return deform_collection

        else:
            base_name = bpy.context.scene.collection.children[0].name
            name_deform = base_name + "_deform"
            deform_collection = bpy.data.collections.new(name_deform)
            bpy.context.scene.collection.children.link(deform_collection)

            return deform_collection


def import_deformers(hierarchy_data, modules_imported):
    modules = hierarchy_data['modules']
    deformers = hierarchy_data['deformers']
    modules_imported = set(modules_imported)

    deform_collection = locate_deform_collection(create=True)

    deformers_required = []
    for name, data in modules.items():
        if (name in modules_imported) and ('deformers' in data):
            deformers_required.extend(data['deformers'])
    deformers_required = set(deformers_required)

    # Penser à ajouter l'instruction qui check si le deformer figure dans un module importé
    for deformer in deformers:
        if (deformer['basis']['name'] in deformers_required) or (bpy.context.scene.rig_transfer.all_differentials):
            if deformer['basis']['type'] == 'MESH':
                import_deform_cage(deformer, deform_collection)
                WARNINGS_COLLECTOR.add_warning("Deformer(s) imported", "Deformer {} was imported".format(deformer['basis']['name']))

            elif deformer['basis']['type'] == 'LATTICE':
                import_lattice(deformer, deform_collection)
                WARNINGS_COLLECTOR.add_warning("Deformer(s) imported", "Deformer {} was imported".format(deformer['basis']['name']))

            else:
                WARNINGS_COLLECTOR.add_warning("Error import deformers", "Impossible to import deformer {}".format(deformer['basis']['name']))

        else:
            WARNINGS_COLLECTOR.add_warning("Deformer skipped", "Deformer {} is not used by any imported module".format(deformer['basis']['name']))