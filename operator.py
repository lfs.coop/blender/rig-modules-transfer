import bpy
import os

from .utils import is_main_armature_active, ENV
from .rig_exporter import rig_exporter
from .rig_importer import rig_importer, rig_apply
from .extract_deformers import export_deform_cage
from .WarningCollector import WARNINGS_COLLECTOR
from .remove_module import remove_module
from .probe_modules import probe_modules, fill_searching_bar, try_detect_folder

class RemoveModuleOperator(bpy.types.Operator):

    """ Remove all bones relative to a module """

    bl_idname = "object.remove_module_bones"
    bl_label = "Remove Module Bones"

    module_name: bpy.props.StringProperty()

    def execute(self, context):
        remove_module(self.module_name)
        return {'FINISHED'}


class ChooseDirectoryOperator(bpy.types.Operator):

    """ Choose the directory where the JSON will be exported """

    bl_idname = "object.select_dir_exp_json"
    bl_label = "Directory of the JSON"

    directory: bpy.props.StringProperty()

    def invoke(self, context, event):
        
        try:
            bpy.ops.object.rig_transfer_fetch_folder()
        except:
            pass
        self.directory = bpy.context.scene.rig_transfer.path_dir_export
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        bpy.context.scene.rig_transfer.path_dir_export = self.directory
        return {'FINISHED'}

class RenameArmatureOperator(bpy.types.Operator):

    """ Rename armature to match conventions """

    bl_idname = "object.rename_armature_export_rig"
    bl_label = "Rename Armature"

    def execute(self, context):
        name_collection = bpy.context.scene.collection.children[0].name
        bpy.context.object.name = name_collection + "_rig"
        return {'FINISHED'}

class TestOperator(bpy.types.Operator):

    """ Test Operator """

    bl_idname = "object.test_export_rig"
    bl_label = "Test"


    def execute(self, context):
        export_deform_cage(None)
        return {'FINISHED'}

class AutoNameExportRig(bpy.types.Operator):

    """ Generate a name for the exported file """

    bl_idname = "object.auto_name_export_rig"
    bl_label = "Auto Name"


    def execute(self, context):
        try:
            context.scene.rig_transfer.export_file_name = context.scene.collection.children[0].name + "_rig_export.json"
        except:
            context.scene.rig_transfer.export_file_name = context.object.name + "_export.json"
        return {'FINISHED'}

class RigExportOperator(bpy.types.Operator):

    """ Transfer a rig or a module from a source to active armature """

    bl_idname = "object.rig_transfer_export"
    bl_label = "Export Rig And Modules"

    @classmethod
    def poll(cls, context):
        return (bpy.context.scene.rig_transfer.valid_export_file) and (bpy.context.scene.rig_transfer.valid_export_dir)

    def execute(self, context):
        WARNINGS_COLLECTOR.reset()
        rig_exporter(context.object)
        WARNINGS_COLLECTOR.export_warnings("warnings_module_exporter.txt")
        return {'FINISHED'}

class RigImportOperator(bpy.types.Operator):

    """ Transfer a rig or a module from a source to active armature """

    bl_idname = "object.rig_transfer_import"
    bl_label = "Import Rig And Modules"

    @classmethod
    def poll(cls, context):
        return bpy.context.scene.rig_transfer.valid_file

    def execute(self, context):
        bpy.context.scene.rig_transfer.probed_data.clear()
        ENV['version_found'] = rig_importer()
        return {'FINISHED'}

class ApplyImportedModules(bpy.types.Operator):

    """ Apply imported armature sub-modules """

    bl_idname = "object.rig_transfer_apply"
    bl_label = "Apply Imported Sub-Modules"

    @classmethod
    def poll(cls, context):
        for item in bpy.context.scene.rig_transfer.probed_data:
            if item.imported:
                return True
        return bpy.context.scene.rig_transfer.all_differentials

    def execute(self, context):
        WARNINGS_COLLECTOR.reset()
        save_state_mirror = context.object.pose.use_mirror_x
        context.object.pose.use_mirror_x = False
        rig_apply()
        context.object.pose.use_mirror_x = save_state_mirror
        WARNINGS_COLLECTOR.export_warnings("warnings_module_importer.txt")
        return {'FINISHED'}

class ProbeModulesOperator(bpy.types.Operator):

    """ Fetch all modules present locally """
    
    bl_idname = "object.rig_transfer_probe_module"
    bl_label = "Probe All Modules"
    
    def execute(self, context):
        found = try_detect_folder()
        if found is not None:
            modules = probe_modules(found)
            fill_searching_bar(modules)
        else:
            return {'CANCELLED'}
        return {'FINISHED'}


class TransferFoundModuleOperator(bpy.types.Operator):

    """ Transfer the chosen module in the 'import' field """

    bl_idname = "object.rig_transfer_transfer_path"
    bl_label = "Transfer Path"

    def execute(self, context):
        data = context.scene.rig_transfer.pool_probe_modules
        
        if len(data.look_for) > 0:
            context.scene.rig_transfer.file_path = data.look_in[data.look_for].full_path
        return {'FINISHED'}


class GoClosestDirOperator(bpy.types.Operator):

    """ Tries to fetch the folder where the JSON should be exported """

    bl_idname = "object.rig_transfer_fetch_folder"
    bl_label = "Get Closest Folder"

    def execute(self, context):
        asset_name = context.scene.collection.children[0].name
        rigmod = try_detect_folder()
        if rigmod is None:
            return {'FINISHED'}
        
        dirs = [d for d in os.listdir(rigmod)]
        dirs_shorts = []
        for d in dirs:
            if d.startswith('lib_chars_'):
                splitted = d.split('_')
                dirs_shorts.append("_".join(splitted[splitted.index('rigmod')+1:]))
        
        if asset_name not in dirs_shorts:
            context.scene.rig_transfer.path_dir_export = rigmod
            return {'FINISHED'}

        path = os.path.join(rigmod, dirs[dirs_shorts.index(asset_name)])

        dirs = os.listdir(path)

        if 'rigmod_json' in dirs:
            path = os.path.join(path, 'rigmod_json')

        context.scene.rig_transfer.path_dir_export = path

        return {'FINISHED'}


